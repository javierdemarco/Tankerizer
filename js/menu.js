var height = this.innerHeight;
var width = this.innerWidth
// We create our only state
var menuState = {

    //Where we set up the game
    create: function () {
        game.world.setBounds(0, 0, game.height, game.width);
        this.wallpaper = game.add.image(0, 0, 'wallpaper');
        this.wallpaper.height = game.height;
        this.wallpaper.width = game.width;
        this.x = game.width / 2;
        this.y = game.height / 2;
        this.soundButton = game.add.button(0, 70, 'audio', this.toggleSound, this);
        if(game.sound.mute){
            this.soundButton.frame = 'audio';
        }
        this.music = game.add.audio('music', 0.8);
        this.music.loop = true;
        this.music.play();
        
        var titleLabel = game.add.text(this.x, 100, 'Tankerizer', {
            font: '80px Special Elite', fill: '#000000'
        });
        titleLabel.anchor.setTo(0.5, 0.5);

        var enterLabel = game.add.text(this.x, this.y, 'Presiona enter para empezar', {
            font: '20px Special Elite', fill: '#000000'
        });
        
        var Controls = game.add.text(0, 200, 'Controles:\nAcelerar: Flecha arriba\nFrenar: Flecha abajo\n'+
                                     'Rotar Derecha: Flecha Derecha\nRotar Izquierda: Flecha Izquierda\nDisparar: Click Izquierdo Raton\n'+
                                     'Pausa: Espacio', { font: '25px Special Elite', fill: '#000000' });
        enterLabel.anchor.setTo(0.5, 0.5);

        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);

        enterKey.onDown.addOnce(this.start, this);
        
        this.soundButton.input.useHandCursor = true;
    },

    start: function () {
        this.music.stop();
        game.state.start('level1', true, false);
    },
    
    toggleSound: function(soundButton){
        game.sound.mute = !game.sound.mute;
        this.soundButton.frame = game.sound.mute ? 'audio':'mute';
    },
};
