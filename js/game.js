var height = this.innerHeight;
var width = this.innerWidth;
var level = 1;
// Initialise Phaser
var game = new Phaser.Game(width, height, Phaser.AUTO, 'gameDiv');

// Define our 'global' variable
game.global = {
    flag : 0
};


// Add all the states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('level1', level1State);
game.state.add('level2', level2State);
// Start the 'boot' state
game.state.start('boot');
