// We create our only state
var loadState = {

    //PreLoad is where we load our assets for this state
    preload: function () {
        var loadingLabel = game.add.text(game.world.centerX, 150, 'Loading...', {
        font: '30px Special Elite', fill: '#ffffff'
        });
        loadingLabel.anchor.setTo(0.5, 0.5);

        var progressBar = game.add.sprite(game.world.centerX, 100, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);
        game.load.tilemap('level1', 'assets/map/mapLvl1.json', null, Phaser.Tilemap.TILED_JSON);
        game.load.tilemap('level2', 'assets/map/mapLvl2.json', null, Phaser.Tilemap.TILED_JSON);
        game.load.atlas('player', '../assets/sprites/tanks.png', '../assets/sprites/tanks.json');
        game.load.atlas('enemy', '../assets/sprites/enemy-tanks.png', '../assets/sprites/tanks.json');
        game.load.image('planeBoss', '../assets/sprites/planeBoss.gif');
        game.load.image('bullet', '../assets/sprites/bullet1.png');
        game.load.image('earth', '../assets/sprites/earth.png');
        game.load.image('snowtile', '../assets/sprites/snowtile.png');
        game.load.image('snowtree', '../assets/sprites/snowtree.png');
        game.load.image('audio', '../assets/sprites/audio.png');
        game.load.image('mute', '../assets/sprites/mute.png');
        game.load.image('grass', '../assets/sprites/grass.png');
        game.load.image('healthPack', '../assets/sprites/healthPack.png');
        game.load.image('Red', '../assets/sprites/Red.png');
        game.load.spritesheet('explosion', '../assets/sprites/explosion.png', 64, 64, 23);
        game.load.image('water', '../assets/sprites/water.png');
        game.load.image('wallpaper', '../assets/menu/wallpaper2.png');
        game.load.audio('music', ['../assets/music/menuMusic.mp3']);
        game.load.audio('movingTank', ['../assets/music/movingTank.mp3']);
		game.load.audio('tankHit', ['../assets/music/tankHit.mp3']);
        game.load.audio('shootTank', ['../assets/music/shootTank.mp3']);
        game.load.audio('heal', ['../assets/music/heal.mp3']);
        game.load.audio('explosion', ['../assets/music/explosion.mp3']);
    },

    //Where we set up the game
    create: function () {
        game.state.start('menu');
    }
};
