// We create our only state
var bootState = {

    //PreLoad is where we load our assets for this state
    preload: function () {

        game.load.image('progressBar', '../assets/sprites/progressBar.png');

    },

    //Where we set up the game
    create: function () {

        //cambiamos el fondo a azul
        game.stage.backgroundColor = '#808080';

        //have the game centered horizontally
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;

        //se incluye un motor de física
        game.physics.startSystem(Phaser.Physics.ARCADE);

        game.state.start('load');
    },
};
