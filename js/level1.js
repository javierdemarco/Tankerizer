var level1State = {
    /**************************
    * Functions used to create Objects *
    ***************************/
    //Enemy tank function, this is where we create the enemy tank
    //Index: position in the enemy Aray
    //x: position x in the map
    //y: position y in the map
    createEnemyTank: function (index, x, y) {

        //Creates the enemy tank in the map
        var enemy = game.add.sprite(x, y, 'enemy', 'tank1');
        enemy.anchor.setTo(0.5, 0.5);
        //Animation
        enemy.animations.add('move', ['tank1', 'tank2', 'tank3', 'tank4', 'tank5', 'tank6'], 20, true);
        //Create Turret sprite
        enemy.turret = game.add.sprite(x, y, 'enemy', 'turret');
        enemy.turret.anchor.setTo(0.3, 0.5);
        enemy.game = game;
        //Create Shadow
        enemy.shadow = game.add.sprite(x, y, 'enemy', 'shadow');
        enemy.shadow.anchor.setTo(0.5, 0.5);
        //Physics and properties
        game.physics.enable(enemy, Phaser.Physics.ARCADE);
        enemy.body.immovable = false;
        enemy.body.collideWorldBounds = true;
        enemy.body.bounce.setTo(1, 1);
        enemy.alive = true;
        enemy.health = 3;
        enemy.nextFire = 0;
        enemy.rateFire = 500;
        enemy.rotation = game.rnd.angle();
        //Index in the enemy Array
        enemy.index = index.toString();
        //  Enemy bullet group
        enemy.bullets = game.add.group();
        enemy.bullets.enableBody = true;
        enemy.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        enemy.bullets.createMultiple(3, 'bullet', 0, false);
        enemy.bullets.setAll('anchor.x', 0.5);
        enemy.bullets.setAll('anchor.y', 0.5);
        enemy.bullets.setAll('outOfBoundsKill', true);
        enemy.bullets.setAll('checkWorldBounds', true);
        enemy.bullets.setAll('alive', false);
        enemy.bringToTop();
        enemy.turret.bringToTop();
        //Create HealthBar
        this.createHealthBar(enemy);
        //Add tank to father Array
        this.enemies.addChild(enemy);
    },
    //Function to Create the enemies in the positions defined
    //element: Object that contains location and properties that will define the enemy
    //group: group of enemies
    //flag: auxiliary variable
    createFromTiledObject: function(element, group, flag) {
        if(flag == 0){
            //Create the sprite
            var sprite = group.create(element.x, element.y, element.properties.sprite);
            //Copy all properties to the sprite
            Object.keys(element.properties).forEach(function(key){
                sprite[key] = element.properties[key];
            });
        }else if (flag == 1){
            //Create the enemy tank
            this.createEnemyTank(this.indexEnemy, element.x, element.y);
            this.indexEnemy++;
        }
    },
    //Function to create all enemies
    createEnemy: function() {
        //Create group
        this.enemies = game.add.group();
        //Physics and propeties
        this.enemies.enableBody = true;
        this.enemies.physicsBodyType = Phaser.Physics.ARCADE;
        this.indexEnemy = 0;
        //Find position where enemies will spawn
        result = this.findObjectsByType('enemy', this.map, 'objectsLayer');
        result.forEach(function(element){
            this.createFromTiledObject(element, this.enemies, 1);
        }, this);
        //Update variables for UI
        this.enemiesAlive = this.enemies.length;
        this.enemiesTotal = this.enemies.length;
    },
    //Function where we create the player
    createPlayer: function(){
        //Find the position in the tilemap where the player will spawn
        var result = this.findObjectsByType('playerStart', this.map, 'objectsLayer')
        //There is just one result, we create the player sprite in that location
        this.player = game.add.sprite(result[0].x, result[0].y, 'player', 'tank1');
        this.player.anchor.setTo(0.5, 0.5);
        //Animation
        this.player.animations.add('move', ['tank1', 'tank2', 'tank3', 'tank4', 'tank5', 'tank6'], 20, true);
        //Physics and properties
        game.physics.enable(this.player, Phaser.Physics.ARCADE);
        this.player.body.drag.set(0.2);
        this.player.enableBody = true;
        this.player.body.maxVelocity.setTo(250, 250);
        this.player.body.collideWorldBounds = true;
        this.player.health = 10;
        this.player.invulnerable = 0;
        this.player.nextFire = 0;
        this.player.fireRate = 200;
        this.player.blinkTime = 100;
        //Create the turret of the player
        this.player.turret = game.add.sprite(0, game.world.centerY, 'player', 'turret');
        this.player.turret.anchor.setTo(0.3, 0.5);
        //Create Shadow of the player
        this.player.shadow = game.add.sprite(0, game.world.centerY, 'player', 'shadow');
        this.player.shadow.anchor.setTo(0.5, 0.5);
        //Create the bullets of the player
        this.player.bullets = game.add.group();
        this.player.bullets.enableBody = true;
        this.player.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.player.bullets.createMultiple(5, 'bullet', 0, false);
        this.player.bullets.setAll('anchor.x', 0.5);
        this.player.bullets.setAll('anchor.y', 0.5);
        this.player.bullets.setAll('outOfBoundsKill', true);
        this.player.bullets.setAll('checkWorldBounds', true);
        this.player.bullets.setAll('alive', false);
        //Bring player and turret top of all other layers
        this.player.bringToTop();
        this.player.turret.bringToTop();
    },
    //Function to create the Boss
    createBoss: function(){
        /*6178*/
         //Creates the enemy tank in the map
        this.planeBoss = game.add.sprite(6178, game.height/2, 'enemy', 'tank1');
        this.planeBoss.anchor.setTo(0.5, 0.5);
        //Animation
        this.planeBoss.animations.add('move', ['tank1', 'tank2', 'tank3', 'tank4', 'tank5', 'tank6'], 20, true);
        //Create Turret sprite
        this.planeBoss.turret = game.add.sprite(6178, game.height/2, 'enemy', 'turret');
        this.planeBoss.turret.anchor.setTo(0.3, 0.5);
        this.planeBoss.game = game;
        //Create Shadow
        this.planeBoss.shadow = game.add.sprite(6178, game.height/2, 'enemy', 'shadow');
        this.planeBoss.shadow.anchor.setTo(0.5, 0.5);
        //Physics and properties
        game.physics.enable(this.planeBoss, Phaser.Physics.ARCADE);
        this.planeBoss.body.immovable = false;
        this.planeBoss.body.collideWorldBounds = true;
        this.planeBoss.body.bounce.setTo(1, 1);
        this.planeBoss.alive = true;
        this.planeBoss.health = 30;
        this.planeBoss.nextFire = 0;
        this.planeBoss.rateFire = 600;
        this.planeBoss.alive = true;
        this.planeBoss.rotation = game.rnd.angle();
        //  Enemy bullet group
        this.planeBoss.bullets = game.add.group();
        this.planeBoss.bullets.enableBody = true;
        this.planeBoss.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.planeBoss.bullets.createMultiple(15, 'bullet', 0, false);
        this.planeBoss.bullets.setAll('anchor.x', 0.5);
        this.planeBoss.bullets.setAll('anchor.y', 0.5);
        this.planeBoss.bullets.setAll('outOfBoundsKill', true);
        this.planeBoss.bullets.setAll('checkWorldBounds', true);
        this.planeBoss.bullets.setAll('alive', false);
        this.planeBoss.bringToTop();
        this.planeBoss.turret.bringToTop();
        //Create HealthBar
        this.createHealthBar(this.planeBoss, 61*this.planeBoss.health, 15);
        this.planeBoss.healthBar.setPosition(5500, game.world.height - 10);
        this.planeBoss.healthBar.fixedToCamera = true;
        this.planeBoss.bringToTop();
        this.planeBoss.turret.bringToTop();        
        this.planeBoss.scale.setTo(3, 3);
        this.planeBoss.turret.scale.setTo(3, 3);
        this.planeBoss.shadow.scale.setTo(3, 3);
        this.planeBoss.healthBar.setFixedToCamera(true);
        //Give enemy diferent color
        var color = Math.random() * 0xffffff;
        this.planeBoss.tint = color;
        this.planeBoss.turret.tint = color;
    },
    //Function Create of the framework Phaser, where we create the whole level
    create: function() {
        //Resize Game World
        game.world.setBounds(-800, -800, 7000, 800);
        //Sound button and music of the game
        this.music = game.add.audio('music', 0.8);
        this.movingTank = game.add.audio('movingTank', 0.03);
        this.music.loop = true;
        this.movingTank.loop = true;
        this.music.play();
        this.movingTank.play();
        this.movingTank.pause();
        this.shootTank = game.add.audio('shootTank', 0.4);
        this.shootTank.play();
        this.shootTank.pause();
        this.healSound = game.add.audio('heal', 0.4);
        this.healSound.play();
        this.healSound.stop();
        this.explosionSound = game.add.audio('explosion', 0.2);
        this.explosionSound.play();
        this.explosionSound.stop();
        //We create the map
        this.map = game.add.tilemap('level1');
        //the first parameter is the tileset name as specified in Tiled, the second is the key to the asset
        this.map.addTilesetImage('earthTile', 'earth');
        this.map.addTilesetImage('waterTile', 'water');
        this.map.addTilesetImage('healthPackTile', 'healthPack');
        this.map.addTilesetImage('grassTile', 'grass');
        this.map.addTilesetImage('startingPoint', 'Red');
        //Create layers of the level
        this.backgroundlayer = this.map.createLayer('backgroundLayer');
        this.blockedLayer = this.map.createLayer('blockedLayer');
        //Collision on blockedLayer
        this.map.setCollisionBetween(1, 2000, true, 'blockedLayer');
        //resizes the game world to match the layer dimensions
        this.backgroundlayer.resizeWorld();
        //Create Items in the level
        this.createItems();
        //Create player
        this.createPlayer();
        //Create Enemy
        this.createEnemy();
        //Create Explosion animation
        this.explosions = game.add.group();
        for (var i = 0; i < 10; i++){
            this.explosionAnimation = this.explosions.create(0, 0, 'explosion', [0], false);
            this.explosionAnimation.anchor.setTo(0.5, 0.5);
            this.explosionAnimation.animations.add('explosion');
        }
        //Force camera to follow player with a deadzone
        game.camera.follow(this.player);
        game.camera.deadzone = new Phaser.Rectangle(150, 150, 500, 300);
        //Event to call function randomRotation every 3 seconds
        game.time.events.loop(Phaser.Timer.SECOND * 2, this.randomRotation, this);
        //Creation of game imputs
        this.cursors = game.input.keyboard.createCursorKeys();
        //Creation of UI
        this.activeBulletsText = game.add.text(0, 0, 'Active Bullets: ' + this.player.bullets.countDead() + ' / ' + this.player.bullets.length
                                           ,{ font: '20px Special Elite', fill: '#ffffff' });
		this.activeBulletsText.fixedToCamera = true;
        this.enemiesText = game.add.text(0, 30, 'Enemies: ' + this.enemiesAlive + ' / ' + this.enemiesTotal, { font: '20px Special Elite', fill: '#ffffff' });
		this.enemiesText.fixedToCamera = true;
        this.LifeText = game.add.text(0, 60, 'Life: ' + this.player.health + ' / ' + 10, { font: '20px Special Elite', fill: '#ffffff' });
		this.LifeText.fixedToCamera = true;
        //Rules to start game and pause gae
        this.spaceKey = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.enterKey = this.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        this.muteKey = this.input.keyboard.addKey(Phaser.Keyboard.M);
        this.spaceKey.onDown.add(this.togglePause, this);
        //Flag to see if boss has spawned of not
        this.flagBoss = 0;
        this.muteKey.onDown.add(this.toggleSound, this);
    },
    //Create item function
    createItems: function() {
        //create items
        this.items = this.game.add.group();
        this.items.enableBody = true;
        var item;
        result = this.findObjectsByType('item', this.map, 'objectsLayer');
        result.forEach(function(element){
          this.createFromTiledObject(element, this.items, 0);
        }, this);
    },
    /*************************
    *Function with update functionality*
    *Contains logic of the level*
    **************************/
    //Update function of phaser framework, it execute 60 times per second
    update: function () {
        //if the game is not paused, the music should be player
        //And turret should be pointing the cursor
        if(!game.physics.arcade.isPaused){
            this.music.resume();
            this.player.turret.rotation = game.physics.arcade.angleToPointer(this.player.turret);
            //If boss hasnt spawn, we have to control the physics of all enemies
            if(this.planeBoss == null){
                //first player vs layers or items
                game.physics.arcade.collide(this.player, this.blockedLayer, null);
                game.physics.arcade.collide(this.enemies, this.blockedLayer, null);
                game.physics.arcade.overlap(this.player, this.items, this.collect, null, this);
                //Then all group of bullets
                this.enemies.forEachAlive(this.enemyCollide, null, this);
                this.enemies.forEachAlive(this.enemyBullet, null, this);
                this.player.bullets.forEachAlive(this.playerBullets, null, this);
                //Function to upadate enemies
                this.enemyUpdate();
            }else {
                //If boss has spawn, we only need to control boss and player physics
                //First bullets
                this.planeBoss.bullets.forEachAlive(this.bossBullets, null, this);
                this.player.bullets.forEachAlive(this.playerBullets, null, this);
                //Then player vs boss or layers
                game.physics.arcade.overlap(this.player, this.planeBoss, null, this.damagePlayer, this);
                game.physics.arcade.collide(this.player, this.blockedLayer, null);
                game.physics.arcade.collide(this.planeBoss, this.blockedLayer, null);
                //If player kilss boss, win condition
                if(this.planeBoss.alive == false){
                    this.winGame();
                }
                this.updateBoss(this.planeBoss, this.player);
            }
            //If mouse is clicked, plyer shoots
            if (game.input.activePointer.isDown && !game.physics.arcade.isPaused){
                this.fire();
            }
            //Function to move player and control imputs
            this.movePlayer();
            //UI to see how many bullets we can shoot
            if(this.activeBulletsText != null){
                this.activeBulletsText.setText('Active Bullets: ' + this.player.bullets.countDead() + ' / ' + this.player.bullets.length);
                this.enemiesText.setText('Enemies: ' + this.enemiesAlive + ' / ' + this.enemiesTotal);
                this.LifeText.setText('Life: ' + this.player.health + ' / ' + 10);
            }
            /*5297*/
            //Condition to spawn boss, when player reaches x position
            if(this.player.x > 5297){
                //First time flahBoss == 0
                if(this.flagBoss < 1){
                    //We kill all enemies
                    this.enemies.forEachAlive(this.killAllEnemies, null, this);
                    //Create boss
                    this.createBoss();
                    //Update counter to move boss and flagBoss to indicate boss has spawn
                    this.counter=0;
                    this.flagBoss++;
                }
            }
        }
        if(this.muteKey.isDown){
            this.toggleSound();
        }
    },
    //Function to move player
    movePlayer: function(){
        //All condition of movement, depends on that the game is not paused
        //Taking imput value we can control player
        //Left or right changes rotation of tank
        if (this.cursors.left.isDown && !game.physics.arcade.isPaused){
            this.player.angle -= 4;
        }
        else if (this.cursors.right.isDown && !game.physics.arcade.isPaused){
            this.player.angle += 4;
        }
        //Up and down, changes velocity
        if (this.cursors.up.isDown){
            this.currentSpeed = 300;
        } else if (this.cursors.down.isDown){
            this.currentSpeed -= 200;
            game.physics.arcade.velocityFromRotation(this.player.rotation, this.currentSpeed, this.player.body.velocity);
        }
        //Condition to control sound
        if (!this.cursors.up.isDown){
            //Movind sound
            this.movingTank.pause();
            game.physics.arcade.velocityFromRotation(this.player.rotation, 0, this.player.body.velocity);
        }
        if (this.currentSpeed > 0){
            if(this.movingTank.paused){
                this.movingTank.resume();
            }
            game.physics.arcade.velocityFromRotation(this.player.rotation, this.currentSpeed, this.player.body.velocity);
        }
        //  Position all the parts and align rotations
        this.player.shadow.x = this.player.x;
        this.player.shadow.y = this.player.y;
        this.player.shadow.rotation = this.player.rotation;
        this.player.turret.x = this.player.x;
        this.player.turret.y = this.player.y;
    },
    //Check if any enemy has collide with player
    enemyCollide: function(enemy, all){
        //If player has colide, damage player
        game.physics.arcade.collide(all.player, enemy, all.damagePlayer, null, all);
    },
    //Check if any enemy bullet has collide with player
    enemyBullet: function(enemy, all){
        //if bullet has collide, damage player
        game.physics.arcade.overlap(enemy.bullets, all.player, all.damagePlayer, null, all);
    },
    //Check if any player bullet has hit an enemy
    playerBullets: function(bullet, all){
        //If bullet has colide, damage enemy
        if(all.planeBoss == null){
            all.enemies.forEachAlive(all.playerEnemyBullet, null, bullet, all);
        }else{
            game.physics.arcade.overlap(bullet, all.planeBoss, all.damageBoss, null, all);
        }
    },
    //Check if player bullet has collide with enemy
    playerEnemyBullet: function(enemy, bullet, all){
        //if it has collide, damage Enemy
        game.physics.arcade.overlap(bullet, enemy, all.damageEnemy, null, all);
    },
    //Function to damage Enemy
    damageEnemy: function(bullet, tank) {
        //Kill bullet sprite
        bullet.kill();
        //Decrease Heatlh
        tank.health -= 1;
        //Recreate HealthBar with less length
        tank.healthBar.kill();
        this.createHealthBar(tank, 20*tank.health, 5);
        //If tank health has reached 0
        if (tank.health <= 0){
            //Kill enemy tank
            tank.alive = false;
            tank.kill();
            tank.turret.kill();
            tank.shadow.kill();
            tank.healthBar.kill();
            //Explosion animation
            this.explosionSound.play();
            var explosion = this.explosions.getFirstExists(false);
            explosion.reset(tank.x, tank.y);
            explosion.play('explosion', 30, false, true);
            //Update enemies alive
            this.enemiesAlive--;
        }
    },
    //Bullet has hit player
    //Player: player of the game
    //Bullet: bullet that has hit the player
    damagePlayer: function(player, bullet) {
        if(bullet.key == "bullet"){
            //First kill bullet sprite
            bullet.kill();
        }
        //Then update invulnerable condition
        //the player has 2 seconds of invulnerability when it gets hit
        if (game.time.now - player.invulnerable > 2000)  {
            //Blink animation of player to indicate that has been hit
            this.blinkPlayer(this.player);
            //Update health and invulnerable time
            player.health--;
            player.invulnerable = game.time.now;
        }
        //If players health reaches 0
        if(player.health == 0){
            //Play explosion animation and sound
            var explosion = this.explosions.getFirstExists(false);
            explosion.reset(player.x, player.y);
            explosion.play('explosion', 30, false, true);
            this.explosionSound.play();
            //KIll player spirte
            player.kill();
            player.turret.kill();
            player.shadow.kill();
            //UI to indicate game over
            this.gameOver = game.add.text(game.camera.x  + 400, game.camera.y + 300, 'GAME OVER', { font: '80px Special Elite', fill: '#ffffff' });
            this.gameOver2 = game.add.text(game.camera.x + 400, game.camera.y + 400, 'Pulsa enter para ir al menu', { font: '40px Special Elite', fill: '#ffffff' });
            //Pause game and go to many when ener is pressed
            this.togglePause();
            this.enterKey.onDown.add(this.goToMenu, this);
        }
    },
    //Function to create Blink animation of player when hit
    blinkPlayer: function(player){
        //change color of player
        player.tint = 0xff0000;
        player.turret.tint = 0xff0000;
        this.timer = game.time.create(false);
        this.timer.loop(0, this.blink2Player, this, player);
        this.timer.start();
    },
    blink2Player: function(player){
        player.tint = 0xffffff;
        player.turret.tint = 0xffffff;
    },
    //Function to update enemies
    enemyUpdate: function() {
        this.enemies.forEachAlive(this.enemyAliveUpdate, null, this.player);
    },
    //For each enemy alive, update it
    //enemy: enemy to update
    //player: the player
    enemyAliveUpdate: function(enemy, player){
        //First the properties
        enemy.shadow.x = enemy.x;
        enemy.shadow.y = enemy.y;
        enemy.shadow.rotation = enemy.rotation;
        enemy.turret.x = enemy.x;
        enemy.turret.y = enemy.y;
        enemy.turret.rotation = game.physics.arcade.angleBetween(enemy, player);
        enemy.healthBar.setPosition(enemy.x, enemy.y - 35);
        //If distance between player and enemy is low, fire
        if (game.physics.arcade.distanceBetween(enemy, player) < 700){
            //Condition to control the fire rate of the enemy
            if (game.time.now > enemy.nextFire && enemy.bullets.countDead() > 0){
                //Update next fire time
                enemy.nextFire = game.time.now + enemy.rateFire;
                //Get Bullet
                var bullete = enemy.bullets.getFirstDead();
                bullete.lifespan = 7000;
                //Reset position
                bullete.reset(enemy.turret.x, enemy.turret.y);
                //Shoot it to player
                bullete.rotation = game.physics.arcade.moveToObject(bullete, player, 200);
            }
        }
        //Adjust velocity
        game.physics.arcade.velocityFromRotation(enemy.rotation, 100, enemy.body.velocity);
    },
    //Function called in event, to change rotation of enemies to a random number
    randomRotation: function() {
        if(this.planeBoss == null){
            //If the game is not paused
            if(!game.physics.arcade.isPaused){
                this.enemies.forEachAlive(this.randomRotAlive);
            }
        }else{
            this.planeBoss.rotation = game.rnd.angle();
        }
    },
    randomRotAlive: function(enemy) {
        enemy.rotation = game.rnd.angle();
    },
    //Function to control the player fire functionallyty
    fire: function() {
        //If player has bullets left and time between them is long enough
        if (game.time.now > this.player.nextFire && this.player.bullets.countDead() > 0){
            //Update next fire time
            this.player.nextFire = game.time.now + this.player.fireRate;
            //Take bullet
            var bullet = this.player.bullets.getFirstExists(false);
            //reset position
            bullet.reset(this.player.turret.x, this.player.turret.y);
            //Rotation to pointer
            bullet.rotation = game.physics.arcade.moveToPointer(bullet, 1000, game.input.activePointer, 500);
            //Sound of shoot
            this.shootTank.play();
        }
    },
    //Health Bar Creation Function
    createHealthBar: function(tank, vwidth, vheight) {
        if(vwidth == null || vheight == null){
            var barConfig = {width: tank.width, height: 5, bar: {color: '#FF0000'}};
        }else{
            var barConfig = {width: vwidth, height: vheight, bar: {color: '#FF0000'}};
        }
        tank.healthBar = new HealthBar(game, barConfig);
        tank.healthBar.setPosition(tank.x, tank.y);
    },
    //Go to meny function
    goToNext: function(){
        this.togglePause();
        game.state.start('level2');
    },
    //Pause function
    togglePause: function () {
        game.sound.onPause = !game.sound.onPause;
        this.music.pause();
        this.movingTank.pause();
        game.physics.arcade.isPaused = (game.physics.arcade.isPaused) ? false : true;
    },
    //find objects in a Tiled layer that containt a property called "type" equal to a certain value
    findObjectsByType: function(type, map, layer) {
        var result = new Array();
        map.objects.objectLayer.forEach(function(element){
            if(element.properties.type === type) {
                element.y -= map.tileHeight;
                result.push(element);
            }
        });
        return result;
    },   
    //Function to collect item
    collect: function(player, collectable) {
        for(var i = 0; i < 3; i++){
            if(player.health < 10){
                player.health++;
                this.healSound.play();
            }
        }
        //remove sprite
        collectable.kill();
    },
    //Win game function
    winGame: function(){
        game.level++;
        this.music.stop();
        this.shootTank.stop();
        this.movingTank.stop();
        this.togglePause();
        this.player.body.velocity = 0;
        this.gameOver = game.add.text(game.camera.x  + 400, game.camera.y + 300, 'HAS GANADO!', { font: '80px Special Elite', fill: '#ffffff' });
        this.gameOver2 = game.add.text(game.camera.x + 400, game.camera.y + 400, 'Pulsa enter para ir al siguiente nivel', { font: '40px Special Elite', fill: '#ffffff' });
        this.enterKey.onDown.add(this.goToNext, this);
    },
    //Function to stop or play sound
    toggleSound: function(){
        game.sound.mute = !game.sound.mute;
    },
    //Function to kill all enemies when boss is going to spawn
    killAllEnemies: function(enemy, all){
        enemy.alive = false;
        enemy.kill();
        enemy.turret.kill();
        enemy.shadow.kill();
        enemy.healthBar.kill();
        all.explosionSound.play();
        var explosion = all.explosions.getFirstExists(false);
        explosion.reset(enemy.x, enemy.y);
        explosion.play('explosion', 30, false, true);
    },
    //Function to update boss
    updateBoss: function(boss, player){
         //First the properties
        boss.shadow.x = boss.x;
        boss.shadow.y = boss.y;
        boss.shadow.rotation = boss.rotation;
        boss.turret.x = boss.x;
        boss.turret.y = boss.y;
        boss.turret.rotation = game.physics.arcade.angleBetween(boss, player);
        //Condition to control the fire rate of the enemy
        if (game.time.now > boss.nextFire && boss.bullets.countDead() > 0){
            //Update next fire time
            boss.nextFire = game.time.now + boss.rateFire;
            //Get Bullet
            var bullete = boss.bullets.getFirstDead();
            //Reset position
            bullete.reset(boss.turret.x, boss.turret.y);
            bullete.scale.setTo(2.5, 2.5);
            bullete.lifespan = 7000;
            //Shoot it to player
            bullete.rotation = game.physics.arcade.moveToObject(bullete, player, 150);
        }
        //Adjust velocity
        game.physics.arcade.velocityFromRotation(boss.rotation, 100, boss.body.velocity);
    },
    //Boss bullet colide checker with player
    bossBullets: function(bullet, all){
        //Check if any player bullet has hit an enemy
        game.physics.arcade.overlap(all.player, bullet, all.damagePlayer, null, all);
    },
    //Function
    damageBoss: function(bullet, boss, all){
        //Kill bullet sprite
        bullet.kill();
        //Decrease Heatlh
        boss.health -= 1;
        //Recreate HealthBar with less length
        boss.healthBar.kill();
        this.createHealthBar(this.planeBoss, 61*this.planeBoss.health, 15);
        this.planeBoss.healthBar.setPosition(5500, game.world.height - 10);
        this.planeBoss.healthBar.fixedToCamera = true;
        //If tank health has reached 0
        if (boss.health <= 0){
            //Kill enemy tank
            boss.alive = false;
            boss.kill();
            boss.shadow.kill();
            boss.turret.kill();
            //Explosion animation
            this.explosionSound.play();
            var explosion = this.explosions.getFirstExists(false);
            explosion.scale.setTo(3, 3);
            explosion.reset(boss.x, boss.y);
            explosion.play('explosion', 30, false, true);
        }
    },
    /***********************
    * Auxiliar functions *
    ***********************/
    //Auxiliar function of phaser's framework to close a state
    shutdown: function(){
        this.player.destroy();
        this.player = null;
        this.enemies.destroy(true);
        this.enemies = null;
        this.planeBoss.destroy();
        this.planeBoss = null;
        this.music.stop();
        this.music = null;
        this.movingTank.stop();
        this.movingTank = null;
        this.healSound.stop();
        this.healSound = null;
        this.explosionAnimation.destroy();
        this.explosionAnimation = null;
        this.explosionSound.stop();
        this.explosionSound = null;
        this.map.destroy();
        this.map = null;
        this.blockedLayer.destroy();
        this.blockedLayer = null;
        this.backgroundlayer.destroy();
        this.backgroundlayer = null;
        this.items.destroy(true);
        this.items = null;
        this.explosions.destroy(true);
        this.explosions = null;
        this.activeBulletsText.destroy();
        this.activeBulletsText = null;
        this.enemiesAlive = 0;
        this.enemiesText.destroy();
        this.enemiesText = null;
        this.enemiesTotal = 0;
        this.LifeText.destroy();
        this.LifeText = null;
        this.flagBoss = 0;
    },
    //Auxiliar function of phaser framework to show debug information
    render: function(){
        /*game.debug.spriteInfo(this.player, 100, 100);*/
        /*game.debug.cameraInfo(game.camera, 32, 32);*/
    },
};
